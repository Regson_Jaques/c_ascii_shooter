#include <iostream>
#include <windows.h>
#include <conio.h>
#include <time.h>

using namespace std;

//variaveis globais
COORD coord = {0, 0}; // sets coordinates to 0,0
bool endGame=false;
bool temTiro=false;
int auxTiro=1;
int ciclos=0;
int tiroAux=0;
bool tiroUnico=false;

void gotoxy (int x, int y)
{
        coord.X = x; coord.Y = y; // X and Y coordinates
        SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

struct nave
{
    // Estruturas das naves
    int posX=25, posY=23, numTiros;

}nave,x1,x2,x3;

void desenhaBordas()
{
    //desenha bordas do game
    int i=0;
    for(i=2;i<50;i++)
    {
        gotoxy(i,2);
        printf("%c",176);
        gotoxy(i,25);
        printf("%c",176);
    }

    for(i=2;i<=25;i++)
    {
        gotoxy(2,i);
        printf("%c",176);
        gotoxy(50,i);
        printf("%c",176);
    }


}

void desenhaNave()
{
    //desenha a nossa nave
    gotoxy(nave.posX-1,nave.posY);
    printf("%c",219);
    gotoxy(nave.posX+1,nave.posY);
    printf("%c",219);
    gotoxy(nave.posX,nave.posY);
    printf("%c",206);
    gotoxy(nave.posX,nave.posY-1);
    printf("%c",30);

}
void desenhaInimigos()
{
    //desenha as 3 naves inimigas de uma vez s�
    gotoxy(x1.posX-1,x1.posY);
    printf("%c",219);
    gotoxy(x1.posX+1,x1.posY);
    printf("%c",219);
    gotoxy(x1.posX,x1.posY);
    printf("%c",206);
    gotoxy(x1.posX,x1.posY+1);
    printf("%c",31);

    gotoxy(x2.posX-1,x2.posY);
    printf("%c",219);
    gotoxy(x2.posX+1,x2.posY);
    printf("%c",219);
    gotoxy(x2.posX,x2.posY);
    printf("%c",206);
    gotoxy(x2.posX,x2.posY+1);
    printf("%c",31);

    gotoxy(x3.posX-1,x3.posY);
    printf("%c",219);
    gotoxy(x3.posX+1,x3.posY);
    printf("%c",219);
    gotoxy(x3.posX,x3.posY);
    printf("%c",206);
    gotoxy(x3.posX,x3.posY+1);
    printf("%c",31);

}
void direcaoInimigos()
{
    // calcula aleatoriamente a dire��o dos inimigos
    srand(time(0));
    int dir=rand()%4;
    switch(dir)
    {
        case 0: if(x1.posX<48) { x1.posX++; } break;
        case 1: if(x1.posX>4) { x1.posX--; } break;
        case 2: if(x1.posY<23) { x1.posY++; } break;
        case 3: if(x1.posY>4) { x1.posY--; } break;

    }

     dir=rand()%4;
    switch(dir)
    {
        case 0: if(x2.posX<48) { x2.posX++; } break;
        case 1: if(x2.posX>4) {  x2.posX--; } break;
        case 2: if(x2.posY<24) { x2.posY++; } break;
        case 3: if(x2.posY>4) {  x2.posY--; } break;

    }

     dir=rand()%4;
    switch(dir)
    {
        case 0: if(x3.posX<48) { x3.posX++; } break;
        case 1: if(x3.posX>4) {  x3.posX--; } break;
        case 2: if(x3.posY<24) { x3.posY++; } break;
        case 3: if(x3.posY>4) {  x3.posY--; } break;

    }

}

void controle()
{
     char x;
    if(kbhit())
    {
        x=getche();
    }

    switch(x)
    {
        case 'a': if(nave.posX>4)  { nave.posX--; } break;
        case 's': if(nave.posY<24) { nave.posY++; } break;
        case 'd': if(nave.posX<48) { nave.posX++; } break;
        case 'w': if(nave.posY>4)  { nave.posY--; } break;
        case 32:  nave.numTiros++; temTiro=true; tiroUnico=true;   break; //spacebar
        case 27: { gotoxy(2,26); exit(0); endGame=true; } break;        //esc
    }
    fflush(stdin);

}

void printPainel()
{
    // imprime informa��es do painel lateral
    gotoxy(52,10);
    printf("ESC para sair");
    gotoxy(52,11);
    printf("Tiros %d", nave.numTiros);
    gotoxy(52,12);
    printf("Ciclos %d", ciclos);

}

void tiro()
{
    // fun��o que cuida do tiro da nave do jogador
    if(temTiro==true)
    {
        if(tiroUnico==true) { tiroAux=nave.posX; tiroUnico=false; }
        auxTiro++;
        gotoxy(tiroAux,nave.posY-auxTiro);
        printf("%c",30);
    }
    if(auxTiro>18)
    {
        temTiro=false;
        auxTiro=1;
        tiroUnico=false;

    }

}

void posicoesIniciais()
{
    // inicializa as naves com posi��es iniciais
    nave.posX=25;
    nave.posY=23;
    x1.posX=25;
    x1.posY=4;
    x2.posX=30;
    x2.posY=7;
    x3.posX=6;
    x3.posY=4;
}

int main()
{

    system("mode 65,28");
    system("title Desafio :::NAVE ASCII:::");
    posicoesIniciais();
    while(endGame==false)
    {

        if(ciclos%2==0) { direcaoInimigos() ;}
        desenhaBordas();
        desenhaNave();
        desenhaInimigos();

        tiro();
        printPainel();
        Sleep(60);
        controle();
        //ClearScreen();
        system("cls");
        //clrscr();

        ciclos++;
    }

    return 0;
}
